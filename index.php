<!DOCTYPE html>
<html lang='fr'>

<head>
    <title>Hello World!</title>
    <meta charset="utf-8" />
</head>

<body>
    <?php include 'data.php';?>
    <!-- Parcours 1er tableaux -->
    <?php foreach ($arr_articles as $key => $perso): ?>

    <h3>Film : <code><?php echo $key; ?></code></h3>

    <dl>
        <?php foreach ($perso as $title => $content): ?>
        <dt>
            <?php echo $title; ?></dt>

        <dd>
            <?php echo $content; ?>
        </dd>
        <?php endforeach;?>
    </dl>

    <?php endforeach;?>

</body>

</html>